import {Component} from '@angular/core';
import {TaskFormComponent} from '../tasks/task-form/task-form.component';
import {Task} from '../shared/model/task.interface';
import {addTask} from '../store/actions/task.actions';
import {Store} from '@ngrx/store';
import {MatDialog} from '@angular/material';
import {State} from '../store/reducers';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {

  constructor(private store: Store<State>,
              private dialog: MatDialog) {}

  public createNewTask(): void {
    const dialogRef = this.dialog.open(TaskFormComponent, {
      width: '800px'
    });

    dialogRef.afterClosed()
      .subscribe((result: Task | undefined) => {
        if (result) {
          this.store.dispatch(addTask( { task: result }));
        }
      });
  }
}
