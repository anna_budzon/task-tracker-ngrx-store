import {createSelector} from '@ngrx/store';
import {State} from '../reducers';

export const selectToDoTasks = (state: State) => state.tasksState.toDoTasks;

export const selectInProgressTasks = (state: State) => state.tasksState.inProgressTasks;

export const selectDoneTasks = (state: State) => state.tasksState.doneTasks;

export const selectLoading = (state: State) => state.tasksState.loading;

export const selectAppState = createSelector(
  selectToDoTasks,
  selectInProgressTasks,
  selectDoneTasks,
  selectLoading,
  (toDo, inProgress, done, loading) => ({
    toDo,
    inProgress,
    done,
    loading
  })
);
