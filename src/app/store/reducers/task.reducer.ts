import {Task} from '../../shared/model/task.interface';
import {createReducer, on} from '@ngrx/store';
import {
  addTask,
  deleteTask,
  getTasks,
  getTasksFailure,
  getTasksSuccess,
  updateTask
} from '../actions/task.actions';
import {TaskStatus} from '../../shared/model/task-status.enum';
import {CategorisedTasks} from '../../shared/model/categorised-tasks';

export interface TaskState {
  toDoTasks: Task[];
  inProgressTasks: Task[];
  doneTasks: Task[];
  loading?: boolean;
}

const initialState: TaskState = {
  toDoTasks: [],
  inProgressTasks: [],
  doneTasks: [],
  loading: true
};

export const taskReducer = createReducer(
  initialState,
  on(getTasks, (state) => ({ ...state, loading: true })),
  on(getTasksSuccess, (state, action) => setCategorisedTasks(state, action)),
  on(getTasksFailure, (state, action) => ({ ...state, error: action.error, loading: false})),
  on(addTask, (state, action) => createTask(state, action)),
  on(updateTask, (state, action) => updateTasks(state, action, true)),
  on(deleteTask, (state, action) => updateTasks(state, action))
);

const setCategorisedTasks = (state: TaskState, action) => {
  switch (action.tasks.status) {
    case TaskStatus.TO_DO:
      state.toDoTasks = [
        ...state.toDoTasks,
        ...action.tasks.tasks
      ];
      break;
    case TaskStatus.IN_PROGRESS:
      state.inProgressTasks = [
        ...state.inProgressTasks,
        ...action.tasks.tasks
      ];
      break;
    case TaskStatus.DONE:
      state.doneTasks = [
        ...state.doneTasks,
        ...action.tasks.tasks
      ];
  }

  return { ...state };
};

const createTask = (state: TaskState, action) => {
  const tasks = addToCategorisedTasks({
    toDo:       state.toDoTasks,
    inProgress: state.inProgressTasks,
    done:       state.doneTasks
  }, action.task);

  return {
    ...state,
    toDoTasks: tasks.toDo,
    inProgressTasks:
    tasks.inProgress,
    doneTasks: tasks.done
  };
};

const addToCategorisedTasks = (tasksGroup: CategorisedTasks, newTask: Task): CategorisedTasks => {
  const categorisedTasks: CategorisedTasks = {
    toDo: tasksGroup.toDo,
    inProgress: tasksGroup.inProgress,
    done: tasksGroup.done
  };

  switch (newTask.status) {
    case TaskStatus.TO_DO:
      categorisedTasks.toDo.push(newTask);
      break;
    case TaskStatus.IN_PROGRESS:
      categorisedTasks.inProgress.push(newTask);
      break;
    case TaskStatus.DONE:
      categorisedTasks.done.push(newTask);
  }

  return categorisedTasks;
};

const updateTasks = (state: TaskState, action: { task: Task, type: string }, update?: boolean) => {
  const tasksOfSameStatus = getTasksOfSameStatus(state, action.task.status);
  const taskToUpdate = tasksOfSameStatus
    .findIndex((item: Task) => item.id === action.task.id);

  if (taskToUpdate !== -1) {
    update ?
      tasksOfSameStatus.splice(taskToUpdate, 1, action.task) :
      tasksOfSameStatus.splice(taskToUpdate, 1);
  }

  return state;
};

const getTasksOfSameStatus = (state: TaskState, status: string): Array<Task> => {
  return status === TaskStatus.TO_DO ? state.toDoTasks :
           (status === TaskStatus.IN_PROGRESS ? state.inProgressTasks : state.doneTasks);
};
