import {ActionReducerMap} from '@ngrx/store';
import {taskReducer, TaskState} from './task.reducer';

export interface State {
  tasksState: TaskState;
}

export const reducers: ActionReducerMap<State> = {
  tasksState: taskReducer
};
