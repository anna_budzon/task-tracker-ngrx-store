import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {
  addTask,
  addTaskFailure,
  addTaskSuccess,
  deleteTask,
  deleteTaskFailure,
  deleteTaskSuccess,
  getTask,
  getTaskFailure,
  getTasks,
  getTasksFailure,
  getTasksSuccess,
  getTaskSuccess,
  updateTask,
  updateTaskFailure,
  updateTaskSuccess
} from '../actions/task.actions';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {TaskService} from '../../tasks/service/task.service';
import {Action} from '@ngrx/store';
import {of} from 'rxjs';
import {NotificationService} from '../../shared/service/notification.service';

@Injectable()
export class TaskEffects {

  constructor(private actions$: Actions,
              private taskService: TaskService,
              private notificationService: NotificationService) {}

  log$ = createEffect(() => this.actions$.pipe(
    tap((action: Action) => console.log(action))
  ), {dispatch: false});

  /**
   * GET TASKS LIST
   */

  getTasks$ = createEffect(() => this.actions$.pipe(
    ofType(getTasks),
    switchMap(() => this.taskService.getGroupedTasks()),
    map(tasks => getTasksSuccess({tasks})),
    catchError((error) => of(getTasksFailure({error: error.message})))
  ));

  getTasksFailure$ = createEffect(() => this.actions$.pipe(
    ofType(getTasksFailure),
    tap(action => {
      this.notificationService.openErrorNotification(`Failed to retrieve tasks: ${ action.error }.`);
    })
  ), {dispatch: false});

  /**
   * GET TASK DETAILS
   */

  getTask$ = createEffect(() => this.actions$.pipe(
    ofType(getTask),
    switchMap(action => this.taskService.getTask(action.id)),
    map(task => getTaskSuccess({task})),
    catchError((error) => of(getTaskFailure({error: error.message})))
  ));

  getTaskSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(getTaskSuccess),
    tap(action => this.taskService.openTaskDetails.next(action.task))
  ), {dispatch: false});

  getTaskFailure$ = createEffect(() => this.actions$.pipe(
    ofType(getTaskFailure),
    tap(action => {
      this.notificationService.openErrorNotification(`Failed to retrieve task: ${ action.error }.`);
    })
  ), {dispatch: false});

  /**
   * CREATE NEW TASK
   */

  addTask$ = createEffect(() => this.actions$.pipe(
    ofType(addTask),
    switchMap(action => this.taskService.createTask(action.task)),
    map(task => addTaskSuccess({task})),
    catchError((error) => of(addTaskFailure({error: error.message})))
  ));

  addTaskSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(addTaskSuccess),
    tap(() => {
      this.notificationService.openSuccessNotification('Task successfully created.');
    })
  ), {dispatch: false});

  addTaskFailure$ = createEffect(() => this.actions$.pipe(
    ofType(addTaskFailure),
    tap(action => {
      this.notificationService.openErrorNotification(`Failed to create task: ${ action.error }.`);
    })
  ), {dispatch: false});

  /**
   * UPDATE TASK
   */

  updateTask$ = createEffect(() => this.actions$.pipe(
    ofType(updateTask),
    switchMap(action => this.taskService.updateTask(action.task)),
    map(() => updateTaskSuccess()),
    catchError((error) => of(updateTaskFailure({error: error.message})))
  ));

  updateTaskSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(updateTaskSuccess),
    tap(() => {
      this.notificationService.openSuccessNotification('Task successfully updated.');
    })
  ), {dispatch: false});

  updateTaskFailure$ = createEffect(() => this.actions$.pipe(
    ofType(updateTaskFailure),
    tap(action => {
      this.notificationService.openErrorNotification(`Failed to update task: ${ action.error }.`);
    })
  ), {dispatch: false});

  /**
   * DELETE TASK
   */

  deleteTask$ = createEffect(() => this.actions$.pipe(
    ofType(deleteTask),
    switchMap(action => this.taskService.deleteTask(action.task.id)),
    map(() => deleteTaskSuccess()),
    catchError((error) => of(deleteTaskFailure({error: error.message})))
  ));

  deleteTaskSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(deleteTaskSuccess),
    tap(() => {
      this.notificationService.openSuccessNotification('Task successfully deleted.');
    })
  ), {dispatch: false});

  deleteTaskFailure$ = createEffect(() => this.actions$.pipe(
    ofType(deleteTaskFailure),
    tap(action => {
      this.notificationService.openErrorNotification(`Failed to delete task: ${ action.error }.`);
    })
  ), {dispatch: false});
}
