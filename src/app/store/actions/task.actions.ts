import {createAction, props} from '@ngrx/store';
import {Task} from '../../shared/model/task.interface';
import {TasksResponse} from '../../shared/model/tasks-response.interface';

export const getTasks = createAction('Get Tasks');
export const getTasksSuccess = createAction('Get Tasks Success', props<{ tasks: TasksResponse }>());
export const getTasksFailure = createAction('Get Tasks Failure', props<{ error: string }>());

export const getTask = createAction('Get Task', props<{ id: string }>());
export const getTaskSuccess = createAction('Get Task Success', props<{ task: Task }>());
export const getTaskFailure = createAction('Get Task Failure', props<{ error: string }>());

export const addTask = createAction('Add Task', props<{ task: Task }>());
export const addTaskSuccess = createAction('Add Task Success', props<{ task: Task }>());
export const addTaskFailure = createAction('Add Task Failure', props<{ error: string }>());

export const updateTask = createAction('Update Task', props<{ task: Task }>());
export const updateTaskSuccess = createAction('Update Task Success');
export const updateTaskFailure = createAction('Update Task Failure', props<{ error: string }>());

export const deleteTask = createAction('Delete Task', props<{ task: Task }>());
export const deleteTaskSuccess = createAction('Delete Task Success');
export const deleteTaskFailure = createAction('Delete Task Failure', props<{ error: string }>());
