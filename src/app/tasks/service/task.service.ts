import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {from, Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {concatMap, finalize, groupBy, map, mergeMap, toArray} from 'rxjs/operators';
import {Task} from '../../shared/model/task.interface';
import {TasksResponse} from '../../shared/model/tasks-response.interface';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  public openTaskDetails: Subject<Task> = new Subject<Task>();

  public loading: Subject<boolean> = new Subject<boolean>();

  public taskDetails$: Observable<Task> = this.openTaskDetails.asObservable();

  public loading$: Observable<boolean> = this.loading.asObservable();

  private baseUrl = environment.BASE_URL;

  private tasksUrl = `${this.baseUrl}/tasks`;

  constructor(private http: HttpClient) {}

  getAllTasks(): Observable<Array<Task>> {
    return this.http.get<Array<Task>>(this.tasksUrl);
  }

  private groupTasks(tasks: Array<Task>): Observable<TasksResponse> {
    return from(tasks)
      .pipe(
        groupBy(task => task.status),
        mergeMap(group => group
          .pipe(
            toArray(),
            map((groupArr) => {
              return {
                status: group.key,
                tasks: groupArr
              };
            })
          )
        )
      );
  }

  getGroupedTasks(): Observable<TasksResponse> {
    return this.getAllTasks()
      .pipe(
        concatMap(tasks => this.groupTasks(tasks)),
        finalize(() => this.loading.next(false))
      );
  }

  getTask(id: string) {
    return this.http.get<Task>(`${ this.tasksUrl }/${ id }`);
  }

  createTask(task: Task) {
    return this.http.post<Task>(`${ this.tasksUrl }/`, task);
  }

  updateTask(task: Task) {
    return this.http.put<Task>(`${ this.tasksUrl }/${ task.id }`, task);
  }

  deleteTask(id: string) {
    return this.http.delete<Task>(`${ this.tasksUrl }/${ id }`);
  }
}
