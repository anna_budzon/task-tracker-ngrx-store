import {Component, Input} from '@angular/core';
import {Task} from '../../shared/model/task.interface';
import {getTask} from '../../store/actions/task.actions';
import {Store} from '@ngrx/store';
import {State} from '../../store/reducers';

@Component({
  selector: 'app-task-card',
  templateUrl: './task-card.component.html',
  styleUrls: ['./task-card.component.scss']
})
export class TaskCardComponent  {
  @Input() task: Task;

  constructor(private store: Store<State>) {}

  public viewDetails() {
    this.store.dispatch(getTask({ id: this.task.id }));
  }
}
