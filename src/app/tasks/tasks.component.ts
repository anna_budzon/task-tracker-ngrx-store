import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDrawer} from '@angular/material';
import {Observable, Subscription} from 'rxjs';
import {TaskService} from './service/task.service';
import {NotificationService} from '../shared/service/notification.service';
import {Task} from '../shared/model/task.interface';
import {Store} from '@ngrx/store';
import {State} from '../store/reducers';
import {deleteTask, getTasks, updateTask} from '../store/actions/task.actions';
import {selectAppState} from '../store/selectors/task.selectors';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit, OnDestroy {

  @ViewChild('drawer', { static: true }) taskDrawer: MatDrawer;

  state$: Observable<{
    toDo: Array<Task>,
    inProgress: Array<Task>,
    done: Array<Task>,
    loading?: boolean,
    error?: string
  }>;

  public toDo: Array<Task> = [];

  public inProgress: Array<Task> = [];

  public done: Array<Task> = [];

  public drawerTask: Task | undefined;

  public loading = true;

  private viewDetailsSub: Subscription;

  constructor(private taskService: TaskService,
              private notificationService: NotificationService,
              private store: Store<State>) {}

  public ngOnInit(): void {
    this.state$ = this.store.select(selectAppState);
    this.state$
      .subscribe((state) => {
        this.toDo = state.toDo;
        this.inProgress = state.inProgress;
        this.done = state.done;
        this.loading = state.loading;
      });

    this.loadTasks();
    this.listenOnTaskDetails();
  }

  private loadTasks(): void {
    this.store.dispatch(getTasks());
    this.taskService.loading$
      .subscribe((isLoading: boolean) => {
        if (!isLoading) {
          this.loading = isLoading;
        }
      });
  }

  public updateTaskStatus(task: Task): void {
    this.taskDrawer.close();
    this.store.dispatch(updateTask({ task }));
  }

  public updateTask(task: Task): void {
    this.store.dispatch(updateTask({ task }));
  }

  public deleteTask(task: Task): void {
    this.store.dispatch(deleteTask({ task }));
  }

  private listenOnTaskDetails(): void {
    this.viewDetailsSub = this.taskService.taskDetails$
      .subscribe((task: Task) => {
        if (task) {
          this.drawerTask = task;
          this.taskDrawer.open();
        }
      });
  }

  public ngOnDestroy(): void {
    if (this.viewDetailsSub) {
      this.viewDetailsSub.unsubscribe();
    }
  }
}
