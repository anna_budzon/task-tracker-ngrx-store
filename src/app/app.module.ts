import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {MaterialModule} from './material/material.module';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MAT_DATE_LOCALE} from '@angular/material';
import {NavigationComponent} from './navigation/navigation.component';
import {TasksModule} from './tasks/tasks.module';
import {AppRoutingModule} from './app-routing/app-routing.module';
import { EffectsModule } from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {reducers} from './store/reducers';
import {TaskEffects} from './store/effects/task.effects';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TasksModule,
    MaterialModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([TaskEffects])
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
