import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../material/material.module';
import {ConfirmationDialogComponent} from './confirmation-dialog/confirmation-dialog.component';
import {StatusPipe} from './pipe/status.pipe';

@NgModule({
  declarations: [
    StatusPipe,
    ConfirmationDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    StatusPipe,
    ConfirmationDialogComponent
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ]
})
export class SharedModule { }
