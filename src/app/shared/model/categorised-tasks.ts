import {Task} from './task.interface';

export class CategorisedTasks {
  toDo: Array<Task>;
  inProgress: Array<Task>;
  done: Array<Task>;
}
