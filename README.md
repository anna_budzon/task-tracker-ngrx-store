# Task Tracker Application using NgRx Store

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.1.

## Development server

Steps to run the Task Tracker application:

* Run command `npm run json-server` in order to have access to mock data.

* Run `ng serve` for a dev server. 

* Navigate to `http://localhost:4200/`.

## Annotation:
Mock data are placed outside `src` folder for purpose, because Angular-CLI is listening on every file change within this folder. Simultaneously,
JSON-Server is updating `mock-tasks.json` file, whenever user interacts with tasks. In order to prevent page from reloading, `mocks` folder has been moved outside `src`.  

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
